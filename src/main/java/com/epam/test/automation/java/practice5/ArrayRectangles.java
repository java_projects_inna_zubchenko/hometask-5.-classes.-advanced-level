package com.epam.test.automation.java.practice5;

public class ArrayRectangles {

    private Rectangle[] rectangleArray;

    public ArrayRectangles(int n){
        rectangleArray = new Rectangle[n];
    }
    public ArrayRectangles(Rectangle... rectangles){
        rectangleArray = rectangles;
    }
    public boolean addRectangle(Rectangle rectangle) {
        for (int i = 0; i < rectangleArray.length; i++)
        {
            if (rectangleArray[i] == null)
            {
                rectangleArray[i] = rectangle;
                return true;
            }
        }
        return false;
    }

    public int numberMaxArea() {
        int number = 0;
        for (int i = 1; i < rectangleArray.length; i++)
            {
                if (rectangleArray[i].area() > rectangleArray[number].area())
                {
                    number=i;
                }
            }
            return number;
    }

    public int numberMinPerimeter() {
        int number = 0;
        for (int i = 1; i<rectangleArray.length; i++)
        {
            if (rectangleArray[i].perimeter()<rectangleArray[number].perimeter())
                number = i;
        }
        return number;
    }

    public int numberSquares() {
       int number = 0;
       for(Rectangle rectangle: rectangleArray){
           if(rectangle.isSquare()){
               number++;
           }
       }
        return number;
    }

}
