package com.epam.test.automation.java.practice5;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class ArrayRectanglesTest {
    @DataProvider(name = "testNumberSquaresWithArray" )
    public Object[][] dataProviderMethod1(){
        return new Object[][] {{new Rectangle[]{new Rectangle(4, 4), new Rectangle(3, 3), new Rectangle(5, 7)} , 2},
                {new Rectangle[]{new Rectangle(7, 1), new Rectangle(3, 3), new Rectangle(5, 7), new Rectangle(4, 3)}, 1},
                {new Rectangle[]{new Rectangle(3, 4), new Rectangle(7, 9), new Rectangle(11, 13)}, 0}};
    }

    @Test(dataProvider = "testNumberSquaresWithArray")
    public void test(Rectangle[] rectangleArray, int expected){
        var actual = new ArrayRectangles(rectangleArray).numberSquares();
        Assert.assertEquals( actual, expected,"The method isSquare returns incorrect value");
    }

    @DataProvider(name = "testNumberSquaresWithRectangles")
    public Object[][] dataProviderMethod2(){
        return new Object[][] {{1, new Rectangle(3, 3), new Rectangle(7, 6)}};
    }

    @Test(dataProvider= "testNumberSquaresWithRectangles")
    public void test2(int expected, Rectangle...rectangles){
        var actual = new ArrayRectangles(rectangles).numberSquares();
        Assert.assertEquals(actual, expected, "The method isSquare returns incorrect value");
    }

    @DataProvider(name = "testNumberMaxArea")
    public Object[][] dataProviderMethod3(){
            return new Object[][]{{2, new Rectangle(1, 3), new Rectangle(2, 2), new Rectangle(3, 10), new Rectangle(5, 1), new Rectangle(1, 2), new Rectangle(3, 3)}};
    }

    @Test(dataProvider = "testNumberMaxArea")
    public void test3(int expected, Rectangle...rectangles){
        var actual = new ArrayRectangles(rectangles).numberMaxArea();
        Assert.assertEquals(actual, expected, "The method numberMaxArea returns incorrect value");
    }
}