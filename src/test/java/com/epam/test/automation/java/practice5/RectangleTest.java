package com.epam.test.automation.java.practice5;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class RectangleTest {
    @DataProvider(name = "testIsSquare" )
    public Object[][] dataProviderMethod(){
        return new Object[][] {{4, 4, true}, {4, 5, false}};
    }

    @Test(dataProvider = "testIsSquare")
    public void test(double a, double b, boolean expected){
        var actual = new Rectangle(a, b).isSquare();
        Assert.assertEquals( actual, expected,"The method isSquare returns incorrect value");
    }
}